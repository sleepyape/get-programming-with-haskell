calcChange owed given = if change > 0
                        then change
                        else 0
    where change = given - owed

inc n = n + 1
double n = n * 2
square n = n * n

ff n = if remaining == 0
       then n - 2
       else 3 * n + 1
    where remaining = mod n 2
